/*
SQLyog Ultimate v12.08 (64 bit)
MySQL - 8.0.19 : Database - task
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE = ''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS */`task` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

/*Table structure for table `sys_task` */

DROP TABLE IF EXISTS `sys_task`;

CREATE TABLE `sys_task`
(
    `id`              varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '任务ID',
    `name`            varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci      DEFAULT '' COMMENT '任务名称',
    `cycle`           varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci      DEFAULT NULL COMMENT '周期(month、week、day、hour、minute、secods)',
    `invoke_target`   varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci     DEFAULT NULL COMMENT '目标字符串',
    `cron_expression` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci     DEFAULT '' COMMENT 'cron执行表达式',
    `policy`          int                                                               DEFAULT NULL COMMENT '计划执行错误策略（1手动，2-自动）',
    `situation`       int                                                               DEFAULT NULL COMMENT '执行情况(1-执行中,2-已暂停)',
    `version`         int                                                               DEFAULT '0' COMMENT '执行版本(每执行一次加一)',
    `last_run_time`   timestamp                                                    NULL DEFAULT NULL COMMENT '上次执行时间',
    `next_run_time`   timestamp                                                    NULL DEFAULT NULL COMMENT '下次执行时间',
    `status`          int                                                               DEFAULT '0' COMMENT '状态（0正常 1暂停）',
    `del_flag`        int                                                               DEFAULT NULL COMMENT '删除标志（0-存在，1-删除）',
    `remark`          varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci     DEFAULT '' COMMENT '备注信息',
    `create_by`       varchar(64)                                                       DEFAULT '' COMMENT '创建者',
    `create_time`     datetime                                                          DEFAULT NULL COMMENT '创建时间',
    `update_by`       varchar(64)                                                       DEFAULT '' COMMENT '更新者',
    `update_time`     datetime                                                          DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci COMMENT ='定时任务调度表';

/*Table structure for table `sys_task_log` */

DROP TABLE IF EXISTS `sys_task_log`;

CREATE TABLE `sys_task_log`
(
    `id`             varchar(32) NOT NULL COMMENT '主键',
    `task_id`        varchar(32) NOT NULL COMMENT '任务日志ID',
    `time`           varchar(20)   DEFAULT NULL COMMENT '耗时(毫秒)',
    `status`         int           DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
    `exception_info` varchar(2000) DEFAULT '' COMMENT '异常信息',
    `create_time`    datetime      DEFAULT NULL COMMENT '创建时间',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci COMMENT ='定时任务调度日志表';

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;
