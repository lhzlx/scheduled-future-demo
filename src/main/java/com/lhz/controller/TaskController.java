package com.lhz.controller;

import com.lhz.model.entity.Task;
import com.lhz.model.param.TaskParam;
import com.lhz.model.vo.TaskVo;
import com.lhz.service.TaskService;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @Author: LiHuaZhi
 * @Date: 2020/2/29 15:42
 * @Description: 定时任务接口
 **/
@RestController("task")
@Api(tags = "任务管理")
public class TaskController {

    @Resource
    private TaskService taskService;

    /**
     * 查询任务列表
     *
     * @return
     */
    @GetMapping("/list")
    @ApiOperation(value = "任务列表", notes = "任务列表", response = Task.class, responseContainer = "List")
    public Object taskList() {
        return taskService.taskList();
    }

    /**
     * 新增任务
     *
     * @param param
     */
    @PostMapping("/add")
    @ApiOperation(value = "新增任务", notes = "新增任务")
    public void addTask(@RequestBody TaskParam param) {
        taskService.addTask(param);
    }

    /**
     * 更新任务
     *
     * @param param
     */
    @PutMapping("/update")
    @ApiOperation(value = "更新任务", notes = "更新任务")
    public void updateTask(@RequestBody TaskParam param) {
        taskService.updateTask(param);
    }

    /**
     * 删除任务
     *
     * @param id
     */
    @DeleteMapping("delete/{id}")
    @ApiOperation(value = "删除任务", notes = "删除任务")
    @ApiImplicitParam(name = "id", value = "任务id", paramType = "path", required = true, dataType = "String")
    public void deleteTask(@PathVariable("id") String id) {
        taskService.deleteTask(id);
    }


    /**
     * 暂停任务
     *
     * @param id
     */
    @PostMapping("stop/{id}")
    @ApiOperation(value = "暂停任务", notes = "暂停任务")
    @ApiImplicitParam(name = "id", value = "任务id", paramType = "path", required = true, dataType = "String")
    public void stopTask(@PathVariable("id") String id) {
        taskService.stopTask(id);
    }


    /**
     * 执行任务
     *
     * @param id
     */
    @PostMapping("invoke/{id}")
    @ApiOperation(value = "执行任务", notes = "执行任务")
    @ApiImplicitParam(name = "id", value = "任务id", paramType = "path", required = true, dataType = "String")
    public void invokeTask(@PathVariable("id") String id) {
        taskService.invokeTask(id);
    }


    /**
     * 查询详情
     *
     * @param id
     * @return
     */
    @GetMapping("info/{id}")
    @ApiOperation(value = "查询详情", notes = "查询详情", response = TaskVo.class)
    @ApiImplicitParam(name = "id", value = "任务id", paramType = "path", required = true, dataType = "String")
    public TaskVo getTaskById(@PathVariable("id") String id) {
        return taskService.getTaskById(id);
    }

}
