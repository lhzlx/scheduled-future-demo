package com.lhz.mapper.task;

import com.lhz.model.entity.Task;
import com.lhz.model.entity.TaskLog;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author: LiHuaZhi
 * @Date: 2020/6/4 22:13
 * @Description:
 **/
@Mapper
public interface TaskMapper {
    List<Task> taskList();

    Task selectTaskById(String id);

    int insert(Task task);

    int update(Task task);

    int updateVersion(@Param("task") Task task, @Param("version") Integer version);

    int deleteTask(String id);

    void insertTaskLog(TaskLog log);

}
