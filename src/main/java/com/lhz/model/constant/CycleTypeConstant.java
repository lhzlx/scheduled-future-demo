package com.lhz.model.constant;

/**
 * @Author: LiHuaZhi
 * @Date: 2020/6/4 22:00
 * @Description:
 **/
public interface CycleTypeConstant {
    String WEEK = "week";
    String MONTH = "month";
    String DAY = "day";
    String HOUR = "hour";
    String MINUTE = "minute";
    String SECODS = "secods";
}
