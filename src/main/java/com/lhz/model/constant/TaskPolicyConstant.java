package com.lhz.model.constant;

/**
 * @Author: LiHuaZhi
 * @Date: 2020/6/4 22:00
 * @Description:
 **/
public interface TaskPolicyConstant {
    Integer MANUAL = 1;
    Integer AUTO = 2;
}
