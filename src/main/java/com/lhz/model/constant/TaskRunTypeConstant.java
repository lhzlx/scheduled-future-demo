package com.lhz.model.constant;

/**
 * @Author: LiHuaZhi
 * @Date: 2020/6/4 22:00
 * @Description:
 **/
public interface TaskRunTypeConstant {
    String SYSTEM_RUN = "sys"; //系统自动执行
    String USER_RUN = "user"; //用户手动触发
}
