package com.lhz.model.constant;

import com.lhz.model.entity.Task;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Author: LiHuaZhi
 * @Date: 2020/6/4 22:00
 * @Description:
 **/
public interface TaskRunnableConstant {

    /**
     * redis锁的key
     */
    String TASK_LOCK_KEY = "task_lock_key:";

    /**
     * 存入被执行的中的task实体 key:taskId,value:task对象
     */
    Map<String, Task> taskMap = new ConcurrentHashMap<>();

    /**
     * 任务计数器，每到20个则清零
     */
    AtomicInteger taskNum = new AtomicInteger(1);
}
