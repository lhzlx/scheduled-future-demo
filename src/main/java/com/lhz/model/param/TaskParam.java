package com.lhz.model.param;

import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.StringUtils;


/**
 * @Author: LiHuaZhi
 * @Date: 2020/6/5 11:38
 * @Description: 新增修改类
 **/
public class TaskParam {
    //任务id
    @ApiModelProperty(value = "任务id")
    private String id;

    //任务名
    @ApiModelProperty(value = "任务名")
    private String name;

    /*
        目标字符串
        格式bean.method(params)
        String字符串类型，包含'、boolean布尔类型，等于true或者false
        long长整形，包含L、double浮点类型，包含D、其他类型归类为整形
     */
    @ApiModelProperty(value = "目标字符串")
    private String invokeTarget;


    //周期(month、week、day、hour、minute、secods)
    @ApiModelProperty(value = "周期(month、week、day、hour、minute、secods)")
    private String cycle;

    //执行策略(1手动，2-自动）
    @ApiModelProperty(value = "执行策略(1手动，2-自动)")
    private Integer policy;

    @ApiModelProperty(value = "按周,* 表示所有")
    private String week;

    @ApiModelProperty(value = "按月,* 表示所有")
    private String month;

    @ApiModelProperty(value = "按日,* 表示所有")
    private String day;

    @ApiModelProperty(value = "按时,* 表示所有")
    private String hour;

    @ApiModelProperty(value = "按分,* 表示所有")
    private String minute;

    @ApiModelProperty(value = "按秒,* 表示所有")
    private String secods;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    //备注
    private String remark;

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInvokeTarget() {
        return invokeTarget;
    }

    public void setInvokeTarget(String invokeTarget) {
        this.invokeTarget = invokeTarget;
    }

    public String getCycle() {
        return cycle;
    }

    public void setCycle(String cycle) {
        this.cycle = cycle;
    }

    public Integer getPolicy() {
        return policy;
    }

    public void setPolicy(Integer policy) {
        this.policy = policy;
    }

    public String getWeek() {
        return week;
    }

    public void setWeek(String week) {
        this.week = week;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getMinute() {
        return minute;
    }

    public void setMinute(String minute) {
        this.minute = StringUtils.isBlank(minute) ? "0" : minute;
    }

    public String getSecods() {
        return secods;
    }

    public void setSecods(String secods) {
        this.secods = StringUtils.isBlank(secods) ? "0" : secods;
    }
}
