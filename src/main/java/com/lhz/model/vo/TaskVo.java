package com.lhz.model.vo;

import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * @Author: LiHuaZhi
 * @Date: 2020/6/5 11:38
 * @Description:
 **/
public class TaskVo {

    //任务名
    @ApiModelProperty(value = "任务名")
    private String name;

    @ApiModelProperty(value = "目标字符串")
    private String invokeTarget;

    @ApiModelProperty(value = "表达式")
    private String cronExpression;

    //周期(month、week、day、hour、minute、secods)
    @ApiModelProperty(value = "周期(month、week、day、hour、minute、secods)")
    private String cycle;

    //执行情况(1-执行中,2-已暂停)
    @ApiModelProperty(value = "执行情况(1-执行中,2-已暂停)")
    private Integer situation;

    @ApiModelProperty(value = "下次执行时间")
    private List<String> next;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInvokeTarget() {
        return invokeTarget;
    }

    public void setInvokeTarget(String invokeTarget) {
        this.invokeTarget = invokeTarget;
    }

    public String getCycle() {
        return cycle;
    }

    public void setCycle(String cycle) {
        this.cycle = cycle;
    }

    public String getCronExpression() {
        return cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    public List<String> getNext() {
        return next;
    }

    public void setNext(List<String> next) {
        this.next = next;
    }

    public Integer getSituation() {
        return situation;
    }

    public void setSituation(Integer situation) {
        this.situation = situation;
    }
}
