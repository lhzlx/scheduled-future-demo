package com.lhz.task;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

/**
 * @Author: LiHuaZhi
 * @Date: 2020/9/25 21:53
 * @Description:
 **/
@Configuration
public class TaskThread {
    @Bean("taskScheduler")
    public ThreadPoolTaskScheduler taskScheduler() {
        ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
        scheduler.setThreadNamePrefix("TaskThread - ");
        return scheduler;
    }
}
